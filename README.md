# Pathful OpenSpout Fork

This is a fork of [OpenSpout v3.7.4](https://github.com/openspout/openspout/tree/v3.7.4) customized to implement 
'grouping' (outlineLevel) as per Explore's needs.

Recently, an [OpenSpout PR](https://github.com/openspout/openspout/pull/113) was created 
for that same goal but it has not been merged yet and will be released on version 4.x which requires PHP 8, so we 
will switch back to OpenSpout (deprecating this fork) when we move to that PHP version.

See original [story](https://pathfulinc.atlassian.net/browse/DEV-552).


## OpenSpout Documentation

OpenSpout Documentation can be found at [https://openspout.readthedocs.io/en/latest/](https://openspout.readthedocs.io/en/latest/).
